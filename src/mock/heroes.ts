export const heroesJson =  [
  {
    "id": 1,
    "name": "Tayassu tajacu",
    "power": "Electricidad",
    "strength": 14,
    "agility": 43
  },
  {
    "id": 2,
    "name": "Oncorhynchus nerka",
    "power": "Hielo",
    "strength": 35,
    "agility": 78
  },
  {
    "id": 3,
    "name": "Macropus rufus",
    "power": "Mental",
    "strength": 4,
    "agility": 47
  },
  {
    "id": 4,
    "name": "Castor fiber",
    "power": "Fuerza",
    "strength": 83,
    "agility": 41
  },
  {
    "id": 5,
    "name": "Papio cynocephalus",
    "power": "Electricidad",
    "strength": 63,
    "agility": 65
  },
  {
    "id": 6,
    "name": "Chauna torquata",
    "power": "Vuelo",
    "strength": 4,
    "agility": 48
  },
  {
    "id": 7,
    "name": "Caiman crocodilus",
    "power": "Electricidad",
    "strength": 25,
    "agility": 49
  },
  {
    "id": 8,
    "name": "Spermophilus armatus",
    "power": "Fuerza",
    "strength": 20,
    "agility": 75
  },
  {
    "id": 9,
    "name": "Meleagris gallopavo",
    "power": "Electricidad",
    "strength": 11,
    "agility": 60
  },
  {
    "id": 10,
    "name": "Petaurus breviceps",
    "power": "Electricidad",
    "strength": 17,
    "agility": 96
  },
  {
    "id": 11,
    "name": "Scolopax minor",
    "power": "Electricidad",
    "strength": 39,
    "agility": 36
  },
  {
    "id": 12,
    "name": "Theropithecus gelada",
    "power": "Hielo",
    "strength": 68,
    "agility": 77
  },
  {
    "id": 13,
    "name": "Lepus arcticus",
    "power": "Hielo",
    "strength": 52,
    "agility": 18
  },
  {
    "id": 14,
    "name": "Ceratotherium simum",
    "power": "Mental",
    "strength": 15,
    "agility": 45
  },
  {
    "id": 15,
    "name": "Geochelone elephantopus",
    "power": "Fuerza",
    "strength": 36,
    "agility": 33
  },
  {
    "id": 16,
    "name": "Canis lupus lycaon",
    "power": "Rapidez",
    "strength": 62,
    "agility": 53
  },
  {
    "id": 17,
    "name": "Macropus agilis",
    "power": "Vuelo",
    "strength": 60,
    "agility": 92
  },
  {
    "id": 18,
    "name": "Bassariscus astutus",
    "power": "Fuerza",
    "strength": 4,
    "agility": 16
  },
  {
    "id": 19,
    "name": "Aegypius tracheliotus",
    "power": "Hielo",
    "strength": 26,
    "agility": 23
  },
  {
    "id": 20,
    "name": "Orcinus orca",
    "power": "Vuelo",
    "strength": 97,
    "agility": 54
  }
];