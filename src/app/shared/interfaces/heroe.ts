export interface Heroe {
  id: number,
  name: string,
  power: string,
  strength: number, 
  agility: number, 
}

export const HEROES_COLUMNS  = [
  'id',
  'name',
  'power',
  'strength',
  'agility',
  'actions'
]

export enum FILTERS  {
  ID = 'id',
  NAME = 'name',
}

export enum MODE {
  ADD = 'add',
  EDIT = 'edit'
}