import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { heroesJson } from 'src/mock/heroes';
import { Heroe } from '../interfaces/heroe';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  selectedHero: Heroe = {
    agility: null,
    id:  null,
    name: null,
    power: null,
    strength: null,
  }
  heroes = heroesJson;
  constructor() { }

  getHeroes() {
    return of(this.heroes);
  }

  getAllHeroes() {
    return this.heroes;
  }

  getHeroeById(id: number) {
    return this.heroes.find(heroe => heroe.id === id);
  }

  getHeroesByName(name: string) {
    return this.heroes.filter(heroe => heroe.name.toLowerCase().indexOf(name.toLowerCase()) !== -1);
  }

  addHeroe(heroe: Heroe) {
    this.heroes.push(heroe);
  }

  resetSelectHeroe(){ 
    this.selectedHero = {
      agility: null,
      id:  null,
      name: null,
      power: null,
      strength: null,
    };
  }

  setHeroeFromList(heroe: Heroe) {
    this.heroes = this.heroes.map(heroeE => {
      if (heroeE.id === heroe.id) {
        return heroe;
      } else {
        return heroeE;
      }
    }
    );
  }

  deleteHeroeFromList(index: number) {
    this.heroes = this.heroes.filter(heroe => heroe.id !== index);
  }

  selectHeroe(heroe: Heroe) {
    this.selectedHero = heroe;
  }

}
