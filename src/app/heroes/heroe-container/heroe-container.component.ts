import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FILTERS, Heroe } from 'src/app/shared/interfaces/heroe';
import { HeroesService } from 'src/app/shared/services/heroes.service';
import { debounceTime, delay, take } from 'rxjs/internal/operators';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-container',
  templateUrl: './heroe-container.component.html',
  styleUrls: ['./heroe-container.component.scss']
})
export class HeroeContainerComponent implements OnInit {
  filterForm: FormGroup;
  subscriptions = new Subscription();
  response: Heroe[];
  constructor(private heroes$: HeroesService,
              private fb: FormBuilder,
              private router: Router,) { }

  ngOnInit(): void {
    this.getHeroes();
    this.loadForm();
    this.subscribeChangesForm();
  }

  addHeroe() {
    this.router.navigate(['heroes/add']);
  }

  private subscribeChangesForm() {
    this.filter(FILTERS.NAME, FILTERS.ID);
    this.filter(FILTERS.ID, FILTERS.NAME);
  }

  private filter(findBy: string, resetId: string) {
    this.subscriptions.add(this.filterForm.get(findBy).valueChanges.pipe(debounceTime(1000)).subscribe(value => {
      if (value !== null) {
        this.filterForm.get(resetId).setValue(null);
        this.filterByField(findBy, value);
      }
    }));
  }

  private filterByField(findBy: string, value: any) {
    if (value === '') this.response = this.heroes$.getAllHeroes(); 
    else {
      switch(findBy) {
        case FILTERS.ID:
          this.response = [this.heroes$.getHeroeById(value)];
          break;
        case FILTERS.NAME: 
          if (value === '') this.response = this.heroes$.getAllHeroes()
          this.response = this.heroes$.getHeroesByName(value);
          break;
      }
    } 
  }

  private loadForm() {
    this.filterForm = this.fb.group({
      name: [''],
      id: ['']
    })
  }

  private getHeroes() {
    this.subscriptions.add(this.heroes$.getHeroes().subscribe(response => {
      this.response = response;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
