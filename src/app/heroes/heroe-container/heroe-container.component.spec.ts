import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroeContainerComponent } from './heroe-container.component';

describe('HeroeContainerComponent', () => {
  let component: HeroeContainerComponent;
  let fixture: ComponentFixture<HeroeContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroeContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroeContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
