import { NgModule } from '@angular/core';
import { HeroesListComponent } from './heroes-list/heroes-list.component';
import { HeroesRoutingModule } from './heroes.routing.module';
import { MaterialModule } from '../material/material.module';
import { HeroeContainerComponent } from './heroe-container/heroe-container.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeroeFormComponent } from './heroe-form/heroe-form.component';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';
import { UppercaseDirective } from '../directives/uppercase.directive';



@NgModule({
  declarations: [
    HeroesListComponent,
    HeroeContainerComponent,
    HeroeFormComponent,
    DialogConfirmComponent,
    UppercaseDirective,
  ],
  imports: [
    HeroesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class HeroesModule { }
