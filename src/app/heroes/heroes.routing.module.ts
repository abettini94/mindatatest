import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateGuard } from '../guards/can-activate.guard';
import { HeroeContainerComponent } from './heroe-container/heroe-container.component';
import { HeroeFormComponent } from './heroe-form/heroe-form.component';


const routes: Routes = [
  {
    path: "", 
    component: HeroeContainerComponent,
  },
  {
    path: "edit", 
    component: HeroeFormComponent,
    canActivate: [CanActivateGuard]
  },
  {
    path: "add", 
    component: HeroeFormComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule { }
