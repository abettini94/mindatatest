import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Heroe, MODE } from 'src/app/shared/interfaces/heroe';
import { HeroesService } from 'src/app/shared/services/heroes.service';

@Component({
  selector: 'app-heroe-form',
  templateUrl: './heroe-form.component.html',
  styleUrls: ['./heroe-form.component.scss']
})
export class HeroeFormComponent implements OnInit {
mode: string;
title: string;
modeEnum = MODE;
heroeForm: FormGroup;
selectedHero: Heroe;
  constructor(private router: Router,
              private fb: FormBuilder,
              private heroes$: HeroesService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.setMode();
    this.title = this.mode === MODE.ADD ? 'Agregar Héroe' : 'Modificar héroe';
    this.loadForm();
  }

  goBack() {
    this.heroes$.resetSelectHeroe();
    this.router.navigate(['/heroes']);
  }

  acceptForm() {
    switch(this.mode) {
      case MODE.ADD: this.addHeroe();
        break;
      case MODE.EDIT: this.editHeroe();
        break;
    }
  }

  addHeroe() {
    const id = this.generateRandomId();
    const heroe = this.createHeroeFromForm(id);
    this.heroes$.addHeroe(heroe);
    this.openSnackBar('Se agrego el héroe exitosamente.')
    this.goBack();
  }

  
  editHeroe() {
    const heroe = this.createHeroeFromForm(this.selectedHero.id);
    this.heroes$.setHeroeFromList(heroe);
    this.openSnackBar('Se editó correctamente el héroe.');
    this.goBack();
  }

  createHeroeFromForm(id: number) {
    const heroe: Heroe = {
      agility: this.heroeForm.get('agility').value,
      name: this.heroeForm.get('name').value,
      power: this.heroeForm.get('power').value,
      strength: this.heroeForm.get('strength').value,
      id: id,
    };
    return heroe;
  }

  private openSnackBar(message: string) {
    this._snackBar.open(message, null, { duration: 2000 });
  }

  private generateRandomId() {
    const id = Math.floor(Math.random() * 500);
    const idExist = this.heroes$.getAllHeroes().find(heroe => heroe.id === id);
    if (!idExist) {
      return id;
    } else {
      this.generateRandomId();
    }
  }

  private setMode() {
    if (this.router.url.indexOf(MODE.ADD) !== -1) this.mode = MODE.ADD
    else this.mode = MODE.EDIT;
  }

  private loadForm() {
    this.selectedHero = this.heroes$.selectedHero;
    this.heroeForm = this.fb.group({
      name: [this.selectedHero.name, [Validators.required]],
      power: [this.selectedHero.power, [Validators.required]],
      strength: [this.selectedHero.strength, [Validators.required, Validators.min(0), Validators.max(100)]],
      agility: [this.selectedHero.agility, [Validators.required, Validators.min(0), Validators.max(100)]],
    })
  }
}
