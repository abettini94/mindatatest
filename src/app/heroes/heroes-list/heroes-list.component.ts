import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Heroe, HEROES_COLUMNS } from 'src/app/shared/interfaces/heroe';
import { HeroesService } from 'src/app/shared/services/heroes.service';
import { DialogConfirmComponent } from '../dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.scss']
})
export class HeroesListComponent implements AfterViewInit {
  displayedColumns: string[] = HEROES_COLUMNS;
  dataSource;
  @Input() set response(value) {
    this.updateTable(value);
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private heroes$: HeroesService,
              private router: Router,
              public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  updateTable(response) {
    this.dataSource = new MatTableDataSource<Heroe[]>(response);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  editHeroe(heroe: Heroe) {
    this.heroes$.selectHeroe(heroe);
    this.router.navigate(['/heroes/edit']);
  }

  confirmDelete(heroe: Heroe) {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '250px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'ok') {
        this.heroes$.deleteHeroeFromList(heroe.id);
        this.updateTable(this.heroes$.getAllHeroes());
      }
    });
  }
}